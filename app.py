from ast import Pass
from curses import termname
from pickle import NONE
from typing import Tuple
from xmlrpc.client import boolean
import psycopg2
import redis
import json
import uuid 
import time
from jinja2 import Template
from flask import Flask, jsonify, request, Response

from send import send_msg_to_rabbitmq

app = Flask(__name__)
r = redis.Redis(
    host='localhost',
    port=6379)



def create_response(
        data: dict = None, status: int = 200, message: str = "", using_cache: boolean = False, tracking_id: str = "",
) -> Tuple[Response, int]:
    if type(data) is not dict and data is not None:
        raise TypeError("Data should be a dictionary 😞")

    response = {
        "code": status,
        "success": 200 <= status < 300,
        "message": message,
        "result": data,
        "using_cache": using_cache,
        "tracking_id": tracking_id
    }
    return jsonify(response), status


"""
~~~~~~~~~~~~ API ~~~~~~~~~~~~
"""
def get_db_connection():
    conn = psycopg2.connect(host='localhost',
                            database='postgres',
                            user='postgres',
                            password='postgres')
    return conn


def response(redis_key):
    redis_value = r.get(redis_key)
    print (redis_value)
    
    if (redis_value != None):
        data = redis_value.decode("utf-8")
        return create_response({"status":data})
    else:
        return create_response({"status":"Not found query!"})

@app.route("/users", methods=['GET', 'POST'])
def users():
    conn = get_db_connection()
    cur = conn.cursor()

    if request.method == 'GET':
        r_key = request.url
        r_value = r.get(r_key)

        if (r_value != None):
            data = json.loads(r_value)
            return create_response({"user":data}, using_cache=True)
        else:
            args = request.args.to_dict()
            print (args)
            
            fields = []
            for key, value in args.items():
                if value != None:
                    fields.append(key)
            if (fields == []):
                cur.execute('SELECT * FROM users;')
            else:
                template = Template(open('get_user_template.txt').read()) 
                cur.execute(template.render(fields = fields, data = args))
            users = cur.fetchall()
            keys = ["id", "age", "name", "team"]
            data = []

            for e in users:
                user = dict(zip(keys, e))
                data.append(user)
            if data == []:
                return create_response({'error': "404: User not found"}, status = 404)

            
            
            r_key = request.url
            r_value = json.dumps(data)
            r.set(r_key, r_value, ex=30)

            cur.close()
            conn.close()
            return create_response({"user":data})

    if request.method == 'POST':
        
        tracking_id = str(uuid.uuid1())
        value = "In-progress"
        r.set(str(tracking_id), value, ex = 1000)

        status = r.get(tracking_id)

        data = request.get_json(force=True)
        query = {"method": "POST", "data": data, "tracking_id":tracking_id}

        body = json.dumps(query)
        send_msg_to_rabbitmq(key = tracking_id, body = body)

        return create_response({"data":status.decode()}, tracking_id=tracking_id, using_cache=True)


#api tracking_id
@app.route("/tracking/<id>", methods = ['GET'])
def tracking_status(id):
    status = r.get(id)
    print (status)
    if (status != None):
        return create_response({"status":status.decode()}, tracking_id=id, using_cache=True) 
    else:
        return create_response({"error":"dont have this tracking id in cache"}, tracking_id=id, using_cache=True) 


def check_if_user_exist(id):
    conn = get_db_connection()
    cur = conn.cursor()

    cur.execute('SELECT COUNT(*) FROM users WHERE id = %s;', id)
    id = cur.fetchone()[0]

    if (id != 0):
        return True
    return False

@app.route("/users/<id>", methods = ['GET', 'PUT' , 'DELETE'])
def users_id(id):
    conn = get_db_connection()
    cur = conn.cursor()

    user_exist = check_if_user_exist(id)

    #GET method
    if (request.method == 'GET'):
        r_key = request.url
        r_value = r.get(r_key)

        #value is in cache
        if (r_value != None):
            data = json.loads(r_value)
            tracking_id = uuid.uuid1()
            print (tracking_id)

            return create_response({"user":data}, using_cache=True)
        #value is not in cache
        else:
            tracking_id = uuid.uuid1()
            send_msg_doing(tracking_id = tracking_id)

            user_exist = check_if_user_exist(id)
            if (user_exist):
                cur.execute('SELECT * FROM users u WHERE u.id = %s;', id)
                resp = cur.fetchone()

                keys = ["id", "age", "name", "team"]
                data = dict(zip(keys, resp))

                r_key = request.url
                r_value = json.dumps(data)
                r.set(r_key, r_value, ex=30)

                cur.close()
                conn.close()
                time.sleep(10) 
                send_msg_done(tracking_id=tracking_id)
                return create_response({"user":data})
            else:
                time.sleep(10) 
                send_msg_done(tracking_id=tracking_id)
                return create_response({'error': "404: User not found"}, status = 404)

        
    
    #PUT method
    if request.method == 'PUT':
        user_exist = check_if_user_exist(id)
        if (user_exist):
            args = request.get_json(force=True)
            print (args)

            fields = []
            for key, value in args.items():
                if value != None:
                    fields.append(key)
            template = Template(open('mod_user_by_ID_template.txt').read()) 
            cur.execute(template.render(fields = fields, data = args, id = id))
            conn.commit()
            
            cur.close()
            conn.close()

            return create_response({'msg': "User is modified successfully."}, status = 201)
        else:
            return create_response({'error': "404: User not found"}, status = 404)
    
    #DELETE method
    if request.method == 'DELETE':
        if (user_exist):
            cur.execute('DELETE FROM users WHERE id = %s;' %id)
            conn.commit()
            cur.close()
            conn.close()
            return create_response({'msg': "User is deleted successfully."})
        else:
            return create_response({'error': "404: User not found"}, status = 404)
        
        




# TODO: Implement the rest of the API here!

"""
~~~~~~~~~~~~ END API ~~~~~~~~~~~~
"""
if __name__ == "__main__":
    app.run(debug=True)
