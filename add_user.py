from ast import Pass
from curses import termname
from pickle import NONE
from typing import Tuple
from xmlrpc.client import boolean
import psycopg2
import redis
import json
import uuid 
import time
from flask import Flask, jsonify, request, Response
import pika, sys, os
from celery import Celery

from send import send_msg_to_rabbitmq

r = redis.Redis(host='localhost', port=6379)
app = Celery('tasks', backend='redis://localhost:6379', broker='pyamqp://guest@localhost//')
#app = Celery('tasks', backend='redis://localhost', broker='pyamqp://')



    
def receive_query():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='POST')

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)
        query = json.loads(body)
        if (query['method'] == "POST"):
            tracking_id = query['tracking_id']
            status = add_user(query['data'])
            print (tracking_id)
            value = "Completed"

            r.set(tracking_id, value)   
            #print (tracking_id, r.get(tracking_id))

    channel.basic_consume(queue='POST', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

def get_db_connection():
    conn = psycopg2.connect(host='localhost',
                            database='postgres',
                            user='postgres',
                            password='postgres')
    return conn


#time.sleep(10)
def add_user(data):
    conn = get_db_connection()
    cur = conn.cursor()

    time.sleep(1000)

    if None not in (data['age'], data['name'], data['team']):
        
        cur.execute('INSERT INTO users (age, name, team)'
            'VALUES (%s, %s, %s)',
            (data['age'], 
            data['name'], 
            data['team'])
            )
        
        conn.commit()
        cur.close()
        conn.close()


        return 201
    else:
        return 422


    
if __name__ == '__main__':
    try:
        receive_query()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)