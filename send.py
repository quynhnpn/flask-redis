#!/usr/bin/env python3.9
import pika

def send_msg_to_rabbitmq(key, body):

    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()


    channel.queue_declare(queue="POST")

    channel.basic_publish(exchange='',
                        routing_key="POST",
                        body=body)
    print(" [x] Sent msg!")

    connection.close()
    return True
